# Logic'Art

"Logic'Art" est une application web permettant d'apprendre la programmation séquentielle, à l'aide d'un langage de programmation visuel (à base de blocs).

Une version en ligne de "Logic'Art" est accessible [ici](https://www.ensciences.fr/addons/logicart/) ou [là](https://thibaultgiauffret.forge.apps.education.fr/logicart/).

**🚧 ATTENTION :** Ce projet est en cours de développement. L'interface reste très basique tant que le fond n'est pas fonctionnel. Évidemment, l'application n'est pas prête pour une utilisation en production...


## 🖥 Tester en local

- Installer NodeJS et NPM
- Installer git : [https://git-scm.com/downloads](https://git-scm.com/downloads)
- Cloner le dépôt : `git clone https://forge.apps.education.fr/thibaultgiauffret/logicart.git`
- Installer les dépendances : `npm install`
- Lancer le serveur local de développement : `npm run test`

## 🛠 Contribuer

Vous pouvez contribuer au projet en proposant des améliorations ou des corrections de bugs [juste là](https://forge.apps.education.fr/thibaultgiauffret/logicart/-/issues) ou en proposant des requêtes de fusion [ici](https://forge.apps.education.fr/thibaultgiauffret/logicart/-/merge_requests).

## ⚖ Licence

"Logic'Art" est sous licence GNU GPL v3. Vous pouvez consulter le texte complet de la licence [ici](https://www.gnu.org/licenses/gpl-3.0.html).