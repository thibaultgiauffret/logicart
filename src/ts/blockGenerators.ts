import { javascriptGenerator } from 'blockly/javascript'
// import * as Blockly from "blockly";

export function setBlocksGen () {
  // Forward
  javascriptGenerator.forward = function () {
    const code = '{ "type": "forward" }'
    return code
  }

  // Backward
  javascriptGenerator.backward = function () {
    const code = '{ "type": "backward" }'
    return code
  }

  // Turn Right
  javascriptGenerator.turn_right = function () {
    const code = '{ "type": "turnRight" }'
    return code
  }

  // Turn Left
  javascriptGenerator.turn_left = function () {
    const code = '{ "type": "turnLeft" }'
    return code
  }

  // Draw red
  javascriptGenerator.draw_red = function () {
    const code = '{ "type": "drawRed" }'
    return code
  }

  // Draw green
  javascriptGenerator.draw_green = function () {
    const code = '{ "type": "drawGreen" }'
    return code
  }

  // Draw blue
  javascriptGenerator.draw_blue = function () {
    const code = '{ "type": "drawBlue"}'
    return code
  }

  // Draw yellow
  javascriptGenerator.draw_yellow = function () {
    const code = '{ "type": "drawYellow" }'
    return code
  }

  // Repeat
  javascriptGenerator.control_repeat = function (block: any) {
    // Get the field number of repeats
    const repeats = block.getFieldValue('TIMES')
    const branch = javascriptGenerator.statementToCode(block, 'DO')
    // Remove \n in branch
    const inlineBranch = branch.replace(/\n/g, '')
    const code = '{ "type": "repeat", "times": ' + repeats + ', "branch": [' + inlineBranch + '] }'
    return code
  }
}
