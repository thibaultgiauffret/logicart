import * as Blockly from 'blockly'

export function setBlockList () {
  /*
  * Actions blocks
  */

  // Go forward
  Blockly.Blocks.forward = {
    init: function () {
      this.appendDummyInput().appendField(Blockly.Msg.FORWARD)
      this.setPreviousStatement(true, null)
      this.setNextStatement(true, null)
      this.setColour('210')
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }

  // Go backward
  Blockly.Blocks.backward = {
    init: function () {
      this.appendDummyInput().appendField(Blockly.Msg.BACKWARD)
      this.setPreviousStatement(true, null)
      this.setNextStatement(true, null)
      this.setColour('210')
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }

  // Turn left
  Blockly.Blocks.turn_left = {
    init: function () {
      this.appendDummyInput().appendField(Blockly.Msg.TURN_LEFT)
      this.setPreviousStatement(true, null)
      this.setNextStatement(true, null)
      this.setColour('210')
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }

  // Turn right
  Blockly.Blocks.turn_right = {
    init: function () {
      this.appendDummyInput().appendField(Blockly.Msg.TURN_RIGHT)
      this.setPreviousStatement(true, null)
      this.setNextStatement(true, null)
      this.setColour('210')
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }

  /*
  * Drawing blocks
  */

  // Draw red
  Blockly.Blocks.draw_red = {
    init: function () {
      this.appendDummyInput().appendField(Blockly.Msg.DRAW_RED)
      this.setPreviousStatement(true, null)
      this.setNextStatement(true, null)
      this.setColour('120')
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }

  // Draw green
  Blockly.Blocks.draw_green = {
    init: function () {
      this.appendDummyInput().appendField(Blockly.Msg.DRAW_GREEN)
      this.setPreviousStatement(true, null)
      this.setNextStatement(true, null)
      this.setColour('120')
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }

  // Draw blue
  Blockly.Blocks.draw_blue = {
    init: function () {
      this.appendDummyInput().appendField(Blockly.Msg.DRAW_BLUE)
      this.setPreviousStatement(true, null)
      this.setNextStatement(true, null)
      this.setColour('120')
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }

  // Draw yellow
  Blockly.Blocks.draw_yellow = {
    init: function () {
      this.appendDummyInput().appendField(Blockly.Msg.DRAW_YELLOW)
      this.setPreviousStatement(true, null)
      this.setNextStatement(true, null)
      this.setColour('120')
      this.setTooltip('')
      this.setHelpUrl('')
    }
  }

  /*
  * Control blocks
  */

  // Repeat
  const repeat = {
    type: 'control_repeat',
    message0: Blockly.Msg.REPEAT + ' %1 ' + Blockly.Msg.TIMES,
    args0: [
      { type: 'field_number', name: 'TIMES', value: 3, min: 0, precision: 1 }
    ],
    message1: '%1',
    args1: [
      { type: 'input_statement', name: 'DO' }
    ],
    previousStatement: null,
    nextStatement: null,
    colour: 330
  }

  Blockly.Blocks.control_repeat = {
    init: function () {
      this.jsonInit(repeat)
      // this.updateShadow();
    }
    // updateShadow: function () {
    //   var connection = this.getInput("TIMES").connection;
    //   var otherConnection = connection.targetConnection;
    //   // Create a shadow block without textToDom

    //   connection.setShadowDom(dom);
    //   connection.respawnShadow_();
    //   connection.connect(otherConnection);
    // },
  }
}
