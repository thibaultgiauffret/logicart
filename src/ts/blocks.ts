/**
 * The blocks.ts file defines custom Blockly blocks and code generators used
 * in the workspace. It imports the necessary modules and functions, and exports
 * a BlockEditor class. The file also defines the toolbox used in the
 * workspace, which contains the custom blocks organized by category.
 */

import * as Blockly from 'blockly'
import * as FrenchLang from './lang/fr'
import { toolbox } from './toolbox'
// JavaScript generator
import { setBlocksGen } from './blockGenerators'

import { setBlockList } from './blockList'
import { javascriptGenerator } from 'blockly/javascript'

export default class BlockEditor {
  private _blockDiv = document.getElementById('blocklyDiv')!
  private _toolbox: any
  public _workspace!: Blockly.WorkspaceSvg
  private _injectionDiv: any
  private _code: string = ''
  private _customDark: any
  private _style: any
  private _toolboxFlag = true

  /**
   * Initializes the Blockly workspace.
   * @async
   * @returns {Promise<void>} A Promise that resolves when the workspace is initialized.
   */
  public async init () {
    this.setDarkTheme()
    this.patchRightBump()
    Blockly.setLocale(FrenchLang)
    this.createWorkspace()
  }

  public createWorkspace () {
    console.log('Creating workspace')
    const options = {
      toolbox,
      collapse: true,
      comments: true,
      disable: true,
      maxBlocks: Infinity,
      trashcan: true,
      horizontalLayout: false,
      toolboxPosition: 'start',
      css: true,
      media: 'https://blockly-demo.appspot.com/static/media/',
      rtl: false,
      scrollbars: true,
      sounds: true,
      oneBasedIndex: true,
      grid: {
        spacing: 20,
        length: 1,
        colour: '#888',
        snap: false
      },
      zoom: {
        controls: true,
        wheel: true,
        startScale: 1,
        maxScale: 3,
        minScale: 0.3,
        scaleSpeed: 1.2
      },
      renderer: 'zelos',
      theme: this._style
    }

    this._workspace = Blockly.inject(this._blockDiv, options)
    this._workspace.addChangeListener(Blockly.Events.disableOrphans)
    this._toolbox = this._workspace.getToolbox()

    this.setBlocksColor()
    this.toolboxStyle()
    this.initTooltips()
    this.setBlocks()

    this._injectionDiv = this._workspace.getInjectionDiv()
    this._injectionDiv.classList.add('rounded-md')

    // refresh the workspace to display the blocks (used to avoid toolbox panel cliping)
    Blockly.svgResize(this._workspace)
  }

  /**
 * Adds a new base block to the Blockly workspace.
 * @returns {void}
 */
  public addBase () {
    const newBlock = this._workspace.newBlock('base')
    newBlock.initSvg()
    newBlock.render()
    this._workspace.centerOnBlock(newBlock.id)
    newBlock.moveBy(0, -100)
  }

  /**
   * Patches the right bump of the Blockly blocks in zelos theme.
   * These patches require modifications to the Blockly lib :
   * blockly/core/renderers/zelos/info.d.ts with adjustXPosition_() from protected to public
   * blockly/core/renderers/common/info.d.ts with makeSpacerRow_ from protected to public, and constants_ from protected to public
   * These modification should be done with patch-package on postinstall via npm install.
   * @returns {void}
   */
  public patchRightBump () {

    // // Removes the auto x adjust of the block's content
    // Blockly.zelos.RenderInfo.prototype.adjustXPosition_ = function () {
    //   // do nothing
    // };

    // Patches the vertical spacing inside blocks to avoid clipping (not working well, creating a vertical shift between start and end of a statement block... wtf ?)
    // const origMakeSpacerRow_ = Blockly.blockRendering.RenderInfo.prototype.makeSpacerRow_;
    // Blockly.blockRendering.RenderInfo.prototype.makeSpacerRow_ = function () {
    //   const spacer = origMakeSpacerRow_.apply(this, [
    //     this.topRow,
    //     this.bottomRow,
    //   ]);
    //   spacer.height/=4;
    //   return spacer;
    // };
  }

  /**
   * Sets the Blockly workspace to use a custom dark theme.
   * @returns {void}
   */
  public setDarkTheme () {
    const blockStyles = {
      hat_blocks: {
        hat: 'cap'
      }
    }

    this._customDark = Blockly.Theme.defineTheme('customdark', {
      name: 'customdark',
      base: Blockly.Themes.Classic,
      componentStyles: {
        workspaceBackgroundColour: '#18181b',
        toolboxBackgroundColour: '#27272a',
        toolboxForegroundColour: '#f',
        flyoutBackgroundColour: '#252526',
        flyoutForegroundColour: '#ccc',
        flyoutOpacity: 0.5,
        scrollbarColour: '#333',
        scrollbarOpacity: 0.5,
        insertionMarkerColour: '#333',
        insertionMarkerOpacity: 1,
        markerColour: '#333',
        cursorColour: '#333',
        selectedGlowColour: '#333',
        selectedGlowOpacity: 1,
        replacementGlowColour: '#333',
        replacementGlowOpacity: 1
      },
      blockStyles
    })
  }

  /**
   * Changes the Blockly workspace theme to the specified theme.
   * @param {string} theme - The name of the theme to change to ("light" or "dark").
   * @returns {void}
   */
  public changeTheme (theme: string) {
    if (theme === 'light') {
      this._style = Blockly.Themes.Classic
    } else if (theme === 'dark') {
      this._style = this._customDark
    }
    this._workspace.setTheme(this._style)
  }

  /**
   * Changes the Blockly workspace theme to the specified theme.
   * @returns {void}
   */
  public toolboxStyle () {
    const titres = [
      'Mouvement',
      'Dessin',
      'Répétition',
      'Autres'
    ]
    const icons = [
      '<i class="fa-solid fa-arrow-up"></i>',
      '<i class="fa-solid fa-paint-brush"></i>',
      '<i class="fa-solid fa-redo"></i>',
      '<i class="fa-solid fa-ellipsis-h"></i>'
    ]
    const category = this._toolbox.getToolboxItems()
    for (let i = 0; i < category.length; i++) {
      if (category[i].rowDiv_ !== undefined) {
        category[i].rowDiv_.getElementsByClassName(
          'blocklyTreeLabel'
        )[0].innerHTML =
          icons[i] +
          '&nbsp;&nbsp;' +
          titres[i]
      }
    }
  }

  /**
   * Generates code from the Blockly workspace.
   * @returns {void}
   */
  public getCode () {
    this._code = ''
    const otherBlocks = this._workspace
      .getTopBlocks(true)
      .filter((block) => block.type !== 'function_def')
    // generate code from allBlocks with blockToCode
    otherBlocks.map((block) => {
      // get code from block function_def even if it is orphan
      const code = javascriptGenerator.blockToCode(block)
      // add to this._code
      this._code += code
      return code
    })
    return this._code
  }

  /**
   * Downloads the Blockly workspace as an XML string.
   * @returns {string} The XML string representing the current state of the workspace.
   */
  public download () {
    const xml = Blockly.Xml.workspaceToDom(this._workspace)
    const xmlText = Blockly.Xml.domToText(xml)
    return xmlText
  }

  /**
   * Uploads the Blockly workspace state from an XML string.
   * @param {any} state - The XML string representing the state of the workspace to upload.
   * @returns {void}
   */
  public upload (state: any) {
    this._workspace.clear()
    const xml = Blockly.utils.xml.textToDom(state)
    Blockly.Xml.domToWorkspace(xml, this._workspace)
    // set workspace center to the top block location
    const topBlocks = this._workspace.getTopBlocks(true)
    if (topBlocks.length > 0) {
      const topBlock = topBlocks[0]
      const topBlockXY = topBlock.getRelativeToSurfaceXY()
      this._workspace.scrollX = -topBlockXY.x
      this._workspace.scrollY = -topBlockXY.y
    }
    this._workspace.getTopBlocks(true)[0].moveBy(50, 50)
  }

  /**
   * Toggles the visibility of the Blockly toolbox.
   * @returns {void}
   */
  public async hideToolbox () {
    if (this._toolboxFlag === false) {
      this._toolboxFlag = true
      this._workspace.getToolbox()!.setVisible(true)
    } else {
      this._toolboxFlag = false
      this._workspace.getToolbox()!.setVisible(false)
      this._workspace.hideChaff()
    }
  }

  /**
   * Undoes the last action in the Blockly workspace.
   * @returns {void}
   */
  public async undo () {
    this._workspace.undo(false)
  }

  /**
   * Redoes the last undone action in the Blockly workspace.
   * @returns {void}
   */
  public async redo () {
    this._workspace.undo(true)
  }

  /**
   * Resets the Blockly workspace to its initial state.
   * @returns {void}
   */
  public async reset () {
    this._workspace.clear()
    this.addBase()
  }

  /**
   * Initializes custom tooltips for Blockly blocks.
   * @returns {void}
   */
  public async initTooltips () {
    const customTooltip = function (div: any, element: any) {
      if (element instanceof Blockly.BlockSvg) {
        div.classList.add('ui-tooltip')
      }
      const tip = Blockly.Tooltip.getTooltipOfObject(element)
      const text = document.createElement('div')
      text.style.color = 'white'
      text.style.fontSize = '14px'
      text.style.padding = '2px'
      text.textContent = tip
      const container = document.createElement('div')
      container.style.display = 'flex'
      container.appendChild(text)
      div.appendChild(container)
    }
    Blockly.Tooltip.setCustomTooltip(customTooltip)
  }

  /**
   * Defines the custom Blockly blocks used in the workspace.
   * @returns {void}
   */
  public setBlocks () {
    // Base block
    Blockly.Blocks.base = {
      init: function () {
        this.setStyle('hat_blocks')
        this.setDeletable(true)
        this.setMovable(false)
        this.setEditable(false)
        this.appendDummyInput().appendField('# Début du programme')
        this.setPreviousStatement(false)
        this.setNextStatement(true, null)
        this.setColour('#F97316')
      }
    }
    javascriptGenerator.base = function (block: any) {
      const code = '{ "type": "base" }'
      return code
    }

    // Add the base block to the workspace
    this.addBase()

    setBlockList()
    setBlocksGen()
  }

  /**
   * Defines the custom colors used for Blockly blocks.
   * @returns {void}
   */
  public setBlocksColor () {
    Blockly.Msg.BKY_ACTION_COLOR = '#42af55'
    Blockly.Msg.BKY_EVENT_COLOR = '#f2b134'
    Blockly.Msg.BKY_CONTROL_COLOR = '#f27474'
    Blockly.Msg.BKY_SENSING_COLOR = '#5c9bd1'
  }
}
