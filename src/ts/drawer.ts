export default class Drawer {
  private _canvas: SVGSVGElement
  private _dim: number
  private _canvasSize: number
  private _drawerObj: SVGSVGElement
  private _drawerObjPos: { x: number, y: number }
  private _drawerObjAngle: number
  private _drawerSize: number
  private _stepCounter: number = 0

  public constructor (canvas: HTMLElement, canvasSize: number, dim: number) {
    // Find the svg element in the canvas
    this._canvas = canvas.querySelector('svg')!
    this._dim = dim
    this._canvasSize = canvasSize
    this._drawerSize = 0.9 * this._canvasSize / this._dim
    this._drawerObjPos = { x: 0, y: 0 }
    this._drawerObjAngle = 0 // Trigonometric angle
    this._drawerObj = this._createDrawer()
    this._stepCounter = 0
  }

  /**
     * Add the drawer to the canvas
     */
  public addDrawer () {
    this._canvas.appendChild(this._drawerObj)
  }

  /**
     * Change the drawer position
     */
  public changeDrawerPosition (x: number, y: number) {
    const rectSize = this._canvasSize / this._dim
    this._drawerObjPos = { x, y }
    this._drawerObj.setAttribute('x', (x * rectSize).toString())
    this._drawerObj.setAttribute('y', (y * rectSize).toString())
  }

  /**
     * Create the drawer
     * @private
     * @returns SVGSVGElement
     */
  private _createDrawer (): SVGSVGElement {
    // Calculate the drawer size
    const rectSize = this._canvasSize / this._dim
    const x = this._drawerObjPos.x
    const y = this._drawerObjPos.y
    // Create the drawer
    const drawerSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    drawerSVG.setAttribute('x', x.toString())
    drawerSVG.setAttribute('y', y.toString())
    drawerSVG.setAttribute('width', rectSize.toString())
    drawerSVG.setAttribute('height', rectSize.toString())
    // Draw a red disc (svg) on the canvas.
    const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
    circle.setAttribute('cx', (rectSize / 2).toString())
    circle.setAttribute('cy', (rectSize / 2).toString())
    circle.setAttribute('r', (this._drawerSize / 2).toString())
    circle.setAttribute('fill', 'red')
    circle.setAttribute('stroke', 'black')
    circle.setAttribute('stroke-width', '1')
    drawerSVG.appendChild(circle)
    // Add an arrow to the drawer (pointing to the right)
    const arrow = document.createElementNS('http://www.w3.org/2000/svg', 'polygon')
    arrow.setAttribute('points', `${(x * rectSize + rectSize / 2 + this._drawerSize / 2).toString()},${(y * rectSize + rectSize / 2)} ${(x * rectSize + rectSize / 2 + this._drawerSize / 4).toString()},${(y * rectSize + rectSize / 2 - this._drawerSize / 4)} ${(x * rectSize + rectSize / 2 + this._drawerSize / 4).toString()},${(y * rectSize + rectSize / 2 + this._drawerSize / 4)}`)
    arrow.setAttribute('fill', 'black')
    arrow.setAttribute('id', 'arrow')
    drawerSVG.appendChild(arrow)

    return drawerSVG
  }

  /**
     * Reset the drawer position
     * @returns void
     */
  public resetDrawer () {
    this.changeDrawerPosition(0, 0)
    this._drawerObjAngle = 0
    this.rotateArrow()
    this._drawerObjPos = { x: 0, y: 0 }
    this._drawerSize = 0.9 * this._canvasSize / this._dim
  }

  /**
   * Clean the canvas
   * @returns void
   * Find each rect element in the canvas and set their fill to none
   */
  public cleanCanvas () {
    const rects = this._canvas.querySelectorAll('rect')
    for (const rect of rects) {
      rect.setAttribute('fill', 'none')
    }
  }

  /**
     * Rotate the arrow of the drawer
     * @returns void
     */
  private rotateArrow () {
    const arrow = this._drawerObj.querySelector('#arrow')!
    // Rotate the arrow with transform, using its center as the rotation point
    arrow.setAttribute('transform', `rotate(${this._drawerObjAngle}, ${this._drawerObjPos.x + this._canvasSize / this._dim / 2}, ${this._drawerObjPos.y + this._canvasSize / this._dim / 2})`)
  }

  /**
     * Change the color of the rectangle at position x, y
     * @param x
     * @param y
     * @param color
     * @returns void
     */
  private _changeColor (x: number, y: number, color: string) {
    const rectSize = this._canvasSize / this._dim
    const rect = this._canvas.querySelector(`rect[x="${x * rectSize}"][y="${y * rectSize}"]`)!
    rect.setAttribute('fill', color)
  }

  /**
     * Execute the code
     * @param code
     * @returns void
     */
  public executeCode (code: string, stepByStep: boolean) {
    // Next lines are not clean... Better way to do it ?
    code = '[' + code + ']'
    // Replace all }{ by },{ to have a valid JSON
    code = code.replace(/}\s*{/g, '},{')
    // Parse the code to JSON
    console.log(code)
    const parsedCode = JSON.parse(code)
    console.log(parsedCode)
    // Remove the first element of the array (the first element is always an empty object)
    parsedCode.shift()
    // Custom eval function
    if (stepByStep) {
      this._eval(parsedCode[this._stepCounter], true)
      this._stepCounter += 1
      if (this._stepCounter > parsedCode.length) {
        this._stepCounter = 0
        this.resetDrawer()
        this.cleanCanvas()
      }
    } else {
      // Reset the drawer position
      this.resetDrawer()
      this.cleanCanvas()
      for (const line of parsedCode) {
        this._eval(line)
      }
    }
  }

  /**
     * Custom eval function
     * @param code
     * @returns void
     * Note : Not using eval() because of safety concerns...
     * First try with a switch case. Slow with loops, but safer. Will improve later.
     */
  private _eval (line: any, stepByStep: boolean = false) {
    if (line === undefined) {
      return
    }
    switch (line.type) {
      case 'forward':
        this.forward()
        break
      case 'backward':
        this.backward()
        break
      case 'turnRight':
        this.turnRight()
        break
      case 'turnLeft':
        this.turnLeft()
        break
      case 'drawRed':
        this.drawRed()
        break
      case 'drawGreen':
        this.drawGreen()
        break
      case 'drawBlue':
        this.drawBlue()
        break
      case 'drawYellow':
        this.drawYellow()
        break
      case 'repeat':
        for (let i = 0; i < line.times; i++) {
          for (const block of line.branch) {
            this._eval(block)
            // // Not clean... Better way to do it ?
            // if (stepByStep) {
            //   // Add a delay of 1s
            //   setTimeout(() => {
            //     // Do nothing
            //   }, 1000)
            // }
          }
        }
        break
    }
  }

  private forward () {
    // Get the position of the drawer
    const x = this._drawerObjPos.x
    const y = this._drawerObjPos.y
    // Move the drawer forward
    if (this._drawerObjAngle === 0) {
      this.changeDrawerPosition(x + 1, y)
    } else if (this._drawerObjAngle === 90) {
      this.changeDrawerPosition(x, y + 1)
    } else if (this._drawerObjAngle === 180) {
      this.changeDrawerPosition(x - 1, y)
    } else if (this._drawerObjAngle === 270) {
      this.changeDrawerPosition(x, y - 1)
    } else {
      console.error('Invalid angle')
    }
  }

  private backward () {
    // Get the position of the drawer
    const x = this._drawerObjPos.x
    const y = this._drawerObjPos.y
    // Move the drawer backward
    if (this._drawerObjAngle === 0) {
      this.changeDrawerPosition(x - 1, y)
    } else if (this._drawerObjAngle === 90) {
      this.changeDrawerPosition(x, y - 1)
    } else if (this._drawerObjAngle === 180) {
      this.changeDrawerPosition(x + 1, y)
    } else if (this._drawerObjAngle === 270) {
      this.changeDrawerPosition(x, y + 1)
    } else {
      console.error('Invalid angle')
    }
  }

  private turnRight () {
    // Turn the drawer right
    this._drawerObjAngle = (this._drawerObjAngle + 90) % 360
    // Rotate the arrow
    this.rotateArrow()
  }

  private turnLeft () {
    // Turn the drawer left
    this._drawerObjAngle = (this._drawerObjAngle + 270) % 360
    // Rotate the arrow
    this.rotateArrow()
  }

  private drawRed () {
    // Get the position of the drawer
    const x = this._drawerObjPos.x
    const y = this._drawerObjPos.y
    // Change the color of underneath the drawer to red
    this._changeColor(x, y, 'red')
  }

  private drawGreen () {
    // Get the position of the drawer
    const x = this._drawerObjPos.x
    const y = this._drawerObjPos.y
    // Change the color of underneath the drawer to green
    this._changeColor(x, y, 'green')
  }

  private drawBlue () {
    // Get the position of the drawer
    const x = this._drawerObjPos.x
    const y = this._drawerObjPos.y
    // Change the color of underneath the drawer to blue
    this._changeColor(x, y, 'blue')
  }

  private drawYellow () {
    // Get the position of the drawer
    const x = this._drawerObjPos.x
    const y = this._drawerObjPos.y
    // Change the color of underneath the drawer to yellow
    this._changeColor(x, y, 'yellow')
  }
}
