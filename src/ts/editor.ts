import GUI from './gui'
import Drawer from './drawer'

export default class Editor {
  private _editor: any
  private _canvas: HTMLElement
  private _canvasSize: number
  // @ts-ignore
  private _gui: GUI
  private _dim: number
  private _drawer: Drawer | null
  private _config: any

  public constructor (gui: GUI) {
    this._gui = gui
    this._dim = 10
    this._canvas = document.getElementById('canvas')! as HTMLElement
    this._canvasSize = 400
    this._drawer = null
    this._config = {}
  }

  /**
     * Initialize the textarea editor.
     */
  public async init () {
    this._editor = document.getElementById('editor')
    this._editor.focus()
    this._setupCanvas()
  }

  private _initConfig () {
    this._config = {
      stepByStep: false,
      dim: 10,
      workspace: this._gui.getBlockXML()
    }
    this.saveConfigToLocalStorage()
  }

  /**
     * Setup the canvas.
     * @private
     * @returns void
    */
  private _setupCanvas (): void {
    // Clear the canvas.
    this.initCanvas()
    // Draw the canvas.
    this.drawCanvas()
    // Create a new drawer and add it to the canvas.
    this._drawer = new Drawer(this._canvas, this._canvasSize, this._dim)
    this._drawer.addDrawer()
    // Initialize the listeners.
    this._initListeners()
  }

  /**
     * Initialize the listeners.
     * @private
     * @returns void
     */
  private _initListeners () {
    // When #dim is changed, update the canvas.
    document.getElementById('dim')!.addEventListener('change', () => {
      this._dim = parseInt((<HTMLInputElement>document.getElementById('dim')!).value)
      this._setupCanvas()
      this._config.dim = this._dim
      this.saveConfigToLocalStorage()
    })

    // When the #play button is clicked, execute the code.
    document.getElementById('play')!.addEventListener('click', () => {
      this._drawer!.executeCode(this.getContent(), this._config.stepByStep)
      this._config.workspace = this._gui.getBlockXML()
      this.saveConfigToLocalStorage()
    })

    // When #step button is clicked, execute the code step by step.
    document.getElementById('step')!.addEventListener('change', () => {
      this._config.stepByStep = !this._config.stepByStep
      this._drawer?.resetDrawer()
      this._drawer?.cleanCanvas()
      // Save the params to local storage
      this.saveConfigToLocalStorage()
    })
  }

  /**
     * Initialize the canvas.
     */
  public initCanvas () {
    this.clearCanvas()
    // Get the dim value
    this._dim = parseInt((<HTMLInputElement>document.getElementById('dim')!).value)
  }

  /**
     * Draw the canvas.
     */
  public drawCanvas () {
    const svgNS = 'http://www.w3.org/2000/svg'
    const svg = document.createElementNS(svgNS, 'svg')
    svg.setAttribute('width', '400')
    svg.setAttribute('height', '400')
    svg.setAttribute('id', 'canvasSVG')

    const lines = this._dim
    const columns = this._dim
    const bw = 400
    const bh = 400
    const px = bw / columns
    const py = bh / lines
    // Find the minimum between px and py.
    const max = Math.max(px, py)

    for (let x = 0; x < bw; x += max) {
      for (let y = 0; y < bh; y += max) {
        const rect = document.createElementNS(svgNS, 'rect')
        rect.setAttribute('x', x.toString())
        rect.setAttribute('y', y.toString())
        rect.setAttribute('width', max.toString())
        rect.setAttribute('height', max.toString())
        rect.setAttribute('stroke', 'black')
        rect.setAttribute('fill', 'none')
        svg.appendChild(rect)
      }
    }

    this._canvas.innerHTML = ''
    this._canvas.appendChild(svg)
  }

  /**
    * Clear the canvas.
    */
  public clearCanvas (): void {
    this._canvas.innerHTML = ''
  }

  /**
     * Get the content of the editor.
     */
  public getContent () {
    return this._editor.value
  }

  /**
     * Set the content of the editor.
     * @param content
     */
  public setContent (content: string) {
    this._editor.value = content
  }

  /**
   * Get the config from local storage.
   * @returns void
   */
  public getConfigFromLocalStorage () {
    // Get the stepByStep value from local storage
    const config = localStorage.getItem('config')
    if (config) {
      console.log('config loaded from local storage')
      this._config = JSON.parse(config)
      // Upload the saved workspace to the block editor
      this._gui.setBlockXML(this._config.workspace)
    } else {
      console.log('config not found in local storage')
      this._initConfig()
    }
    // Set the dim input value
    document.getElementById('dim')!.setAttribute('value', this._config.dim)
    // Set the stepByStep bootstrap switch state
    const stepSwitch = document.getElementById('step') as HTMLInputElement
    stepSwitch.checked = this._config.stepByStep
  }

  /**
   * Save the config to local storage.
   * @returns void
   */
  public saveConfigToLocalStorage () {
    localStorage.setItem('config', JSON.stringify(this._config))
  }
}
