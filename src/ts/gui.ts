import '@fontsource/open-sans'
import '../css/style.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap'
import '@fortawesome/fontawesome-free/css/all.css'

import BlockEditor from './blocks'
import Editor from './editor'

export default class GUI {
  private _blockEditor: BlockEditor
  private _editor: Editor

  constructor () {
    this._blockEditor = new BlockEditor()
    this._editor = new Editor(this)
  }

  /**
     * Initialize the GUI.
     */
  async init (options = null) {
    try {
      await this._init(options)
    } catch (error) {
      console.error(error)
    }
  }

  /**
    * Internal GUI init.
    * It calls setupUI then loadInitialContent.
    */
  async _init (options = null) {
    await this.setupUI(options)
  }

  /**
     * The setupUI method is a protected method of the GUI class that sets up the user interface
     * (UI) of the application. It initializes the different components of the GUI, loads the
     * script to execute, and adds event listeners to the different components.
     * @param options
     */
  protected async setupUI (options: any) {
    // Init of the components
    await Promise.all([
      this._blockEditor.init(),
      this._editor.init()
    ])

    // Triggers a code update when mouse is up on blockly workspace div
    this._blockEditor._workspace.addChangeListener(() => {
      if (this._blockEditor._workspace.isDragging()) return
      this.updateCode()
    })

    // Update the GUI with preferences stored in local storage
    this._editor.getConfigFromLocalStorage()
  }

  /**
     * Get the block editor code.
     */
  public async updateCode () {
    const code = this._blockEditor.getCode()
    this._editor.setContent(code)
  }

  /**
     * Reset the block editor.
     */
  public async resetBlock () {
    this._blockEditor.reset()
  }

  /**
   * Get the block editor XML
   * @returns string
   **/
  public getBlockXML () {
    return this._blockEditor.download()
  }

  /**
   * Set the block editor from XML
   */
  public setBlockXML (xml: string) {
    this._blockEditor.upload(xml)
  }
}
