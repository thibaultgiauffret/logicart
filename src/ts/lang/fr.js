/* eslint-disable */
(function (root, factory) {
    if (typeof define === "function" && define.amd) {
      // AMD
      define([], factory);
    } else if (typeof exports === "object") {
      // Node.js
      module.exports = factory();
    } else {
      // Browser
      var messages = factory();
      for (var key in messages) {
        root.Blockly.Msg[key] = messages[key];
      }
    }
  })(this, function () {
    // This file was automatically generated.  Do not modify.
  
    "use strict";
  
    var Blockly = Blockly || { Msg: Object.create(null) };
  
    // Actions
    Blockly.Msg["FORWARD"] = "Avancer";
    Blockly.Msg["BACKWARD"] = "Reculer";
    Blockly.Msg["TURN_RIGHT"] = "Droite";
    Blockly.Msg["TURN_LEFT"] = "Gauche";

    // Draw 
    Blockly.Msg["DRAW_RED"] = "Colorier en rouge";
    Blockly.Msg["DRAW_BLUE"] = "Colorier en bleu";
    Blockly.Msg["DRAW_GREEN"] = "Colorier en vert";
    Blockly.Msg["DRAW_YELLOW"] = "Colorier en jaune";

    // Controls
    Blockly.Msg["REPEAT"] = "Répéter";
    Blockly.Msg["TIMES"] = "fois";

    return Blockly.Msg;
  });
  