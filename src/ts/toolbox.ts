/**
 * The toolbox.ts file defines the custom toolbox used in the Blockly workspace.
 * It exports a toolbox object that contains the categories and blocks in the toolbox.
 * Each category is defined as a category object with a name, color, and contents array
 * of blocks. Each block is defined as a block object with a type.
 */

const toolbox = {
  kind: 'categoryToolbox',
  contents: [
    {
      kind: 'category',
      name: 'Actions',
      colour: '210',
      contents: [
        {
          kind: 'block',
          type: 'forward'
        },
        {
          kind: 'block',
          type: 'backward'
        },
        {
          kind: 'block',
          type: 'turn_left'
        },
        {
          kind: 'block',
          type: 'turn_right'
        }
      ]
    },
    {
      kind: 'category',
      name: 'Drawing',
      colour: '120',
      contents: [
        {
          kind: 'block',
          type: 'draw_red'
        },
        {
          kind: 'block',
          type: 'draw_green'
        },
        {
          kind: 'block',
          type: 'draw_blue'
        },
        {
          kind: 'block',
          type: 'draw_yellow'
        }
      ]
    },
    {
      kind: 'category',
      name: 'Control',
      colour: '330',
      contents: [
        {
          kind: 'block',
          type: 'control_repeat'
        }
      ]
    },
    {
      kind: 'category',
      name: 'Others',
      colour: '210',
      contents: [
        {
          kind: 'block',
          type: 'base'
        }
      ]
    }
  ]
}

export { toolbox }
